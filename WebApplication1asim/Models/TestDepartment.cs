﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1asim.Models
{
    public class TestDepartment
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}