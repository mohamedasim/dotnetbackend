﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using WebApplication1asim.Models;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace WebApplication1asim.Controllers
{
    public class TestEmployeeController : ApiController
    {
        // GET: TestEmployee
        public HttpResponseMessage Get()
        {
            string query = @"
            select EmployeeId,EmployeeName,Department,
            convert(varchar(10),DateOfJoining,120) as DateOfJoining,
            PhotoFileName
            from 
            dbo.TestEmployee
            ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }
        public string Post(TestEmployee emp)
        {
            try
            {
                string query = @"
                             insert into dbo.TestEmployee values
                             (
                                '" + emp.EmployeeName + @"'
                                ,'" + emp.Department + @"'
                                ,'" + emp.DateOfJoining + @"'
                                ,'" + emp.PhotoFileName + @"'
                                )
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Added Successfully!";
            }
            catch (Exception)
            {
                return "failed to Add!";
            }
        }

        public string Put(TestEmployee emp)
        {
            try
            {
                string query = @"
                             update  dbo.TestEmployee set 
                             EmployeeName='" + emp.EmployeeName + @"'
                             ,Department='" + emp.Department + @"'
                             ,DateOfJoining='" + emp.DateOfJoining + @"'
                             ,PhotoFileName='" + emp.PhotoFileName + @"'
                             where EmployeeId=" + emp.EmployeeId + @"
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Updated Successfully!";
            }
            catch (Exception)
            {
                return "failed to update!";
            }
        }
        public string Delete(int id)
        {
            try
            {
                string query = @"
                             delete from dbo.TestEmployee
                             where EmployeeId=" + id + @"
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Deleted Successfully!";
            }
            catch (Exception)
            {
                return "failed to Delete!";
            }
        }
        [System.Web.Http.Route("api/TestEmployee/GetAllDepartmentNames")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAllDepartmentNames()
        {
            string query = @"
                             select DepartmentName from dbo.TestDepartment";

            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }
        [Route("api/TestEmployee/SaveFile")]
        public string SaveFile()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = HttpContext.Current.Server.MapPath("//Photos//" + filename);

                postedFile.SaveAs(physicalPath);

                return filename;
            }
            catch (Exception ex)
            {
                return "anonymous.png";
            }
        }
    }
}