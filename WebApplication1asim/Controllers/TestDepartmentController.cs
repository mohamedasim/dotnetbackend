﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using WebApplication1asim.Models;

namespace WebApplication1asim.Controllers
{
    public class TestDepartmentController : ApiController
    {
        // GET: TestDepartment
        public HttpResponseMessage Get()
        {
            string query = @"
            select DepartmentId,DepartmentName from 
            dbo.TestDepartment
            ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }
        public string Post(TestDepartment dep)
        {
            try
            {
                string query = @"
                             insert into dbo.TestDepartment values
                             ('" + dep.DepartmentName + @"')
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Added Successfully!";
            }
            catch (Exception)
            {
                return "failed to Add!";
            }
        }

        public string Put(TestDepartment dep)
        {
            try
            {
                string query = @"
                             update  dbo.TestDepartment set DepartmentName=
                             '" + dep.DepartmentName + @"'
                             where DepartmentId=" + dep.DepartmentId + @"
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Updated Successfully!";
            }
            catch (Exception)
            {
                return "failed to update!";
            }
        }
        public string Delete(int id)
        {
            try
            {
                string query = @"
                             delete from dbo.TestDepartment
                             where DepartmentId=" + id + @"
                        ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
                return "Deleted Successfully!";
            }
            catch (Exception ex)
            {
                return "failed to Delete!";
            }
        }
    }
}